The Bank App
================================================

.. _about:
This is a app created using Django and PostgreSQL running with Docker.


Quick start
-------------------------------------------

Running the application requires Docker to be installed locally. For installation guides, visit: https://docs.docker.com/installation/.

Clone this repository:

.. code-block:: 

   $ mkdir my-clone
   $ cd my-clone/
   $ git clone https://gitlab.com/szangyi/dev-env-mandatory-2.git 



Run the project in Docker's virtual enviroment:

.. code-block:: 

   $ docker-compose up
