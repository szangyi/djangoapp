Welcome to our Bank App Project Documentation
=========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   1-about
   2-the-project
   3-requirements
   4-workflow
   5-sqa
